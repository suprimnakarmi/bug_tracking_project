# frozen_string_literal: true

class Project < ApplicationRecord
  validates :project_name, presence: true
  has_many :bugs, dependent: :destroy
end
