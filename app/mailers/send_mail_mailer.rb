# frozen_string_literal: true

class SendMailMailer < ApplicationMailer
  default from: 'bordanakarmi1@gmail.com'

  def submission
    @projects = Project.all
    mail(to: 'nanjel14@gmail.com', subject: 'Projects list')
  end
end
