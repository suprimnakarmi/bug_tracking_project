# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'brodanakarmi1@gmail.com'
  layout 'mailer'
end
