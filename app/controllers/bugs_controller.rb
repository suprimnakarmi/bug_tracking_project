# frozen_string_literal: true

class BugsController < ApplicationController
  def index; end

  def show
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.find(params[:id])
  end

  def new
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.new
    @users_array = User.all.map { |user| [user.email, user.email] }
  end

  def create
    @project = Project.find(params[:project_id])
    @bug = @project.bugs.new(bug_params)
    if @bug.save
      redirect_to project_bug_path(@project, @bug)
    else
      render 'new'
    end
  end

  def edit
   end

  def update
   end

  def destroy
    @project = Project.find(params[:id])
    @bug = @project.bugs.find(params[:id])
    @bug.destroy
    redirect_to project_path(@project)
  end

  private

  def bug_params
    params.require(:bug).permit(:buglist, :assigned_to)
  end
end
