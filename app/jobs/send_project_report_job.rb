# frozen_string_literal: true

class SendProjectReportJob < ApplicationJob
  queue_as :default

  def perform
    # Do something later
    SendMailMailer.submission.deliver
  end
end
